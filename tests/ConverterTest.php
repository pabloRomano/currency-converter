<?php

namespace CurrencyConverter\Tests;

use PHPUnit\Framework\TestCase;
use CurrencyConverter\CurrencyConverter;
use CurrencyConverter\Currencies\PesoArgentino;
use CurrencyConverter\Currencies\UsDollar;
use CurrencyConverter\Currencies\Euro;

/**
 * Class ConverterTest.
 *
 * @package CurrencyConverter\Tests
 */
class ConverterTest extends TestCase {
  
  protected $converter;

  /**
   * Setup the converter instance
   */
    public function setUp(): void
    {
      $this->converter = new CurrencyConverter();
    }
    
    /** @test */
    public function it_can_sum_same_currency_types_and_return_in_dolars(){
      
      $currencyTypeToReturn = 'USD';

      $result = $this->converter->sumOfCoins( 
        $currencyTypeToReturn, 
        new PesoArgentino(10), 
        new PesoArgentino(20),
        new PesoArgentino(35),
        new PesoArgentino(15),
        new PesoArgentino(20)
      );

      $this->assertEquals( 2.3 , round($result, 2));
    }

    /** @test */
    public function it_can_sum_different_currency_types_and_return_in_pesos() {
      $currencyTypeToReturn = 'ARS';

      $result = $this->converter->sumOfCoins(
        $currencyTypeToReturn,
        new Euro(10),
        new UsDollar(109),
        new PesoArgentino(10000)
      );

      $this->assertEquals(15200.16, round($result, 2));
    }

    /** @test */
    public function it_throws_exception_when_the_currency_type_to_return_is_not_found(){
      
      $this->expectExceptionMessage("Currency type not found");
      
      $currencyTypeToReturn = 'BRL';

      $result = $this->converter->sumOfCoins(
        $currencyTypeToReturn,
        new Euro(50),
        new UsDollar(333),
        new PesoArgentino(1)
      );
    }

}