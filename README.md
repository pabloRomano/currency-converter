
# Currency Converter (Proyecto desarrollado para entrevista técnica)
  
  * Consigna: El ejercicio consiste en la recepción de una o varias "monedas" con su valor y su tipo (Peso, Dolar y Euro a modo de ejemplo) y la devolución es la suma total de las mismas en un tipo en particular especificado.

  * A modo de ejemplo coloqué las tasas de cambio fijas, pero en un caso real se deberían obtener de un web service por ejemplo la api de yahoo finance o similar, si es necesario puedo implementarlo.

  * Cualquier modificación, cambio o sugerencia quedo a dispoción.

# Para ejecutar los tests
`$ composer install`
`$ ./vendor/bin/phpunit`
