<?php

namespace CurrencyConverter\Currencies;

use CurrencyConverter\CurrencyInterface, 
    CurrencyConverter\ExchangeRatesProvider;


class PesoArgentino implements CurrencyInterface
{
    protected $amount;
    protected $type;

    /**
     * Instanciate the object whit his type and amount
     *
     * @param Float $amount
     */
    public function __construct(Float $amount)
    {
        $this->amount = $amount;
        $this->type = 'ARS';
    }
    
    /**
     * Perform the convertion to the target currency type
     *
     * @param String $currencyType
     * @return Float
     */
    public function convertTo(String $currencyType) : Float 
    {
        return $this->amount * ExchangeRatesProvider::getRate($this->type, $currencyType);
    }
}