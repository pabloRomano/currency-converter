<?php

namespace CurrencyConverter;

class CurrencyConverter
{
    /**
     * Perform the sum of all the coins in $coinsInput
     *
     * @param String $currencyTypeToReturn
     * @param CurrencyInterface ...$coinsInput
     * @return Float
     */
    public function sumOfCoins(String $currencyTypeToReturn, CurrencyInterface ...$coinsInput) : Float
    {   
        $total = 0;
        
        foreach ($coinsInput as $coin) {
            $total += $coin->convertTo($currencyTypeToReturn);
        }
         
        return $total;
    }
}
