<?php

namespace CurrencyConverter;

class ExchangeRatesProvider
{   
    /**
     * Return the convertion rate for one currency to another
     *
     * @param  String $from
     * @param  Mixed  $to
     * @return Float
     */
    public static function getRate(String $from, String $to) : Float
    {   
        $rates = array(
            "USD" => array(
                        "ARS" => 43.24,
                        "EUR" => 1.12,
                        "USD" => 1
            ), 
            "EUR" => array(
                        "ARS" => 48.7,
                        "EUR" => 1,
                        "USD" => 0.88
            ),
            "ARS" => array(
                        "ARS" => 1,
                        "EUR" => 0.020,
                        "USD" => 0.023
            )
        );

        if( isset($rates[$from][$to]) ){
            return $rates[$from][$to];
        }else{
            throw new \Exception("Currency type not found");
        } 
    }
}
