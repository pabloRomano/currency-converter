<?php

namespace CurrencyConverter;

/**
 * Interface CurrencyInterface.
 *
 */
interface CurrencyInterface
{   
    /**
     * Perform the convertion of the currency to the target type $currencyType
     *
     * @param String $currencyType
     * @return Float
     */
    public function convertTo(String $currencyType) : Float;
}